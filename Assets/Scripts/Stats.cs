﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public float Power;
    public float Precision;
    public float Toughness;
    public float Vitality;
    public float MaxHP;
    public float CurrentHP;

    void Start()
    {
        SetMaxHp();

        CurrentHP = MaxHP;

        UIController UI = UIController.Instance;
        UI.UpdateHealth(CurrentHP, MaxHP);
        UI.UpdatePow(Power);
        UI.UpdatePrec(Precision);
        UI.UpdateTough(Toughness);
        UI.UpdateVit(Vitality);

    }

    public void TakeDamage(float dmg)
    {
        CurrentHP -= dmg;

        UIController.Instance.UpdateHealth(CurrentHP, MaxHP);
    }

    public void Heal(float amount)
    {
        UIController.Instance.UpdateHealth(CurrentHP, MaxHP);
    }

    public void SetMaxHp()
    {
        MaxHP = Vitality * 10;
    }
}
