﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorsEmblem : Item
{
    public GameObject Player;
    public UIController ui;

    void OnEnable()
    {
        Player.GetComponent<Stats>().Power += 100;
        ui.UpdatePow(Player.GetComponent<Stats>().Power);
    }

    /*void OnDisable()
    {
        Player.GetComponent<Stats>().Power -= 100;
    }*/
}
