﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buff : MonoBehaviour
{
    public int StackCount;
    public int StackLimit;
    public int Duration;
    public int MaxDuration;
    public bool IsStackable;
    public string Name;

    void OnEnable()
    {
        StartCoroutine(DecreaseDuration());
    }

    IEnumerator DecreaseDuration()
    {
        while(Duration != 0)
        {
            Duration--;
            Debug.Log(Duration);
            yield return new WaitForSeconds(1.0f);
        }

        this.gameObject.SetActive(false);
    }
    
}
