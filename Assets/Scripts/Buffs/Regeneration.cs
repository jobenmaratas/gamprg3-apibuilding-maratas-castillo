﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regeneration : Buff
{
    void Awake()
    {
        Name = "Regeneration";
        IsStackable = false;
        Duration = 5;
        MaxDuration = 10;
    }
    public void UseBuff()
    {
        Debug.Log("Regeneration Buff Active");
    }
}
