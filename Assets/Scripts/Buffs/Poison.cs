﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : Buff
{
    void Awake()
    {
        Name = "Poison";
        IsStackable = true;
        StackLimit = 5;
    }
    public void UseBuff()
    {
        if (StackCount >= StackLimit)
        {
            StackCount = StackLimit;
        }

        Debug.Log("Poison Buff Active");
    }
}
