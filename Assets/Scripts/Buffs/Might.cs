﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Might : Buff
{
    void Awake()
    {
        Name = "Might";
        IsStackable = true;
        StackLimit = 10;
    }
    public void UseBuff()
    {
        if (StackCount >= StackLimit)
        {
            StackCount = StackLimit;
        }

        Debug.Log("Might Buff Active");
    }
}
