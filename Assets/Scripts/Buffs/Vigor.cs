﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vigor : Buff
{
    void Awake()
    {
        Name = "Vigor";
        IsStackable = false;
        Duration = 5;
        MaxDuration = 10;
    }
    public void UseBuff()
    {
        Debug.Log("Vigor Buff Active");
    }
}
