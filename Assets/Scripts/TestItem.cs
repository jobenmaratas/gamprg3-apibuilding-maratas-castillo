﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestItem : MonoBehaviour
{
    public GameObject Target;
    public Item item;
    private Inventory inventory;
    public void AddItem()
    {
        Item newItem = Instantiate(item);
        inventory = Target.GetComponent<Inventory>();
        inventory.AddItem(item);
    }
}
