﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public string ItemName;
    public string ItemDescription;
    public bool IsUsable;
    public bool IsStackable;
    public int CurrentStack;
    public int MaxStack;
}

