﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffReceiver : MonoBehaviour
{
    [SerializeField] List<Buff> Buffs;
    public Buff TestBuff;
    public Buff TestBuff2;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            ApplyBuff(TestBuff);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            ApplyBuff(TestBuff2);
        }

    }

    public void ApplyBuff(Buff buff)
    {
        if (Buffs.Count <= 0)
        {
            Buff curBuff = Instantiate(buff);
            curBuff.gameObject.SetActive(true);
            curBuff.StackCount++;
            Buffs.Add(curBuff);
            return;
        }

        foreach (Buff obj in Buffs)
        {
            if(obj.Name == buff.Name)
            {
                // Resets the duration of the buff
                if(!obj.IsStackable)
                {
                    obj.Duration = buff.MaxDuration;
                    return;
                }

                // Adds stack
                else
                {
                    obj.StackCount++;

                    if(obj.StackCount >= obj.StackLimit)
                    {
                        obj.StackCount = obj.StackLimit;
                        obj.Duration = buff.MaxDuration * buff.StackLimit;

                        Debug.Log(obj.Duration);

                        return;
                    }

                    else
                    {
                        obj.Duration += buff.Duration;
                        Debug.Log(obj.Duration);

                        return;
                    }         
                }
            }            
        }   

        if(!Buffs.Contains(buff))
        {
            Buff curBuff = Instantiate(buff);
            curBuff.gameObject.SetActive(true);
            curBuff.StackCount++;
            Buffs.Add(curBuff);
        }
    }
}
