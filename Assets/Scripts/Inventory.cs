﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> ListOfItems; // All possible items
    public int MaxSlots; // Max count for inventory

    void Start()
    {
        ListOfItems = new List<Item>();
    }

    // Add item to inventory
    public void AddItem(Item item)
    {
        if (ListOfItems.Count <= MaxSlots)
            return;

        for (int i = 0; i < ListOfItems.Count; i++)
        {
            if (item.ItemName == ListOfItems[i].ItemName)
            {
                if(ListOfItems[i].CurrentStack >= item.MaxStack)
                {
                    ListOfItems.Add(item);
                    return;
                }

                ListOfItems[i].CurrentStack++;
                return; 
            }
        }

        ListOfItems.Add(item);
    }

    // Remove an item from inventory
    public void RemoveItem(Item item)
    {
        if (ListOfItems.Count <= 0)
            return;

        for (int i = 0; i < ListOfItems.Count; i++)
        {
            if (item.ItemName == ListOfItems[i].ItemName)
            {
                if (item.IsStackable && item.CurrentStack > 0)
                {
                    item.CurrentStack--;
                    if (item.CurrentStack <= 0)
                    {
                        ListOfItems[i] = null;
                        return;
                    }
                }
                
                else
                {
                    ListOfItems[i] = null;
                }
            }
        }
    }
}

