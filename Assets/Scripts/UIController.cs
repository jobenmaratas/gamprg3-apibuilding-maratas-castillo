﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController Instance;

    public Text Pow;
    public Text Prec;
    public Text Tough;
    public Text Vit;
    public Text Health;

    void Start()
    {
        Instance = this;
    }

    public void UpdatePow(float count)
    {
        Pow.text = (int)count + "";
    }

    public void UpdatePrec(float count)
    {
        Prec.text = (int)count + "";
    }

    public void UpdateTough(float count)
    {
        Tough.text = (int)count + "";
    }

    public void UpdateVit(float count)
    {
        Vit.text = (int)count + "";
    }

    public void UpdateHealth(float cur, float max)
    {
        Health.text = (int)cur + "/" + (int)max;
    }
}
